<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-md-6 col-12 col-post">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<img src="<?= postThumb($args['post']); ?>" alt="post-image" class="post-image-card">
				<?php endif; ?>
			</a>
			<div class="post-card-content">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
				<a href="<?= $link; ?>" class="square-item post-card-link">
					<img src="<?= ICONS ?>arrow-black-left.png" alt="arrow-left">
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
