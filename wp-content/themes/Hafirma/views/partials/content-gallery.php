<?php if (isset($args['gallery']) && $args['gallery']) : ?>
	<section class="gallery-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'הגלריה שלנו'; ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="row-gallery-slider arrows-slider arrows-slider-gallery">
			<div class="base-gallery-slider" dir="rtl">
				<?php foreach ($args['gallery'] as $item) : ?>
					<div class="p-1">
						<a href="<?= $item['url']; ?>" class="gallery-main-item"
						   style="background-image: url('<?= $item['url']; ?>')" data-lightbox="gallery">
									<span class="square-item view-more">
										<img src="<?= ICONS ?>plus.png" alt="view-more">
									</span>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="container-fluid">
			<?php if (isset($args['link']) && $args['link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $args['link']['url'];?>" class="base-link">
								<span class="link-text"><?= (isset($args['link']['title']) && $args['link']['title'])
										? $args['link']['title'] : 'לגלריה המלאה';
									?></span>
							<img src="<?= ICONS ?>arrow-left.png" alt="arrow-left">
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
