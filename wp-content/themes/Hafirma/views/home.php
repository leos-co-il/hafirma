<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$main_img = (isset($fields['main_back']) && isset($fields['main_back']['url'])) ? $fields['main_back']['url'] :
		(has_post_thumbnail() ? postThumb() : '');
?>
<section class="first-screen">
	<?php if ($fields['main_back'] || has_post_thumbnail()) : ?>
		<div class="home-image" style="background-image: url('<?= $main_img; ?>')"></div>
	<?php endif;
	if ($fields['offer_title'] || $fields['offer_text']) : ?>
		<div class="offer-block-home">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="offer-block-inside">
							<div class="row justify-content-between align-items-center">
								<div class="col-xl-6 col-lg-8 col-12">
									<div class="row align-items-end">
										<?php if ($logo = opt('foo_logo')) : ?>
											<div class="col-auto">
												<a class="logo offer-logo"><img src="<?= $logo['url']; ?>" alt="logo-hafirma"></a>
											</div>
										<?php endif; if ($fields['offer_title']) : ?>
											<div class="col-xl-auto col-12">
												<h2 class="form-title mb-3"><?= $fields['offer_title']; ?></h2>
											</div>
										<?php endif; ?>
									</div>
									<?php if ($fields['offer_text']) : ?>
										<div class="row">
											<div class="col-12">
												<div class="base-output"><?= $fields['offer_text']; ?></div>
											</div>
										</div>
									<?php endif; ?>
								</div>
								<div class="col-lg-4 col-12">
									<div class="pop-trigger offer-link">
										<span class="offer-link-text">אשמח שתחזרו אלי ליעוץ</span>
										<img src="<?= ICONS ?>arrow-left.png">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php if ($fields['h_cats']) : ?>
	<section class="categories-block">
		<div class="container">
			<?php if ($fields['h_cats_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="block-title">
							<?= $fields['h_cats_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch home-cat-row">
				<?php foreach ($fields['h_cats'] as $i => $cat) {
					get_template_part('views/partials/card', 'category', [
							'category' => $cat,
					]);
				} ?>
			</div>
		<?php if ($fields['h_cats_link']) : ?>
			<div class="row justify-content-end">
				<div class="col-auto">
					<a href="<?= $fields['h_cats_link']['url'];?>" class="base-link">
						<span class="link-text"><?= (isset($fields['h_cats_link']['title']) && $fields['h_cats_link']['title'])
								? $fields['h_cats_link']['title'] : 'לכל הקטגוריות';
							?></span>
						<img src="<?= ICONS ?>arrow-black-left.png" alt="arrow-left">
					</a>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/content', 'reviews',
		[
				'title' => $fields['reviews_title'],
				'subtitle' => $fields['reviews_subtitle'],
				'link' => $fields['reviews'],
		]);
?>
<div class="form-home">
	<?php if ($img = opt('base_form_img')) : ?>
		<img src="<?= $img['url']; ?>" alt="image" class="base-form-img">
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-start">
			<div class="col-12">
				<div class="row justify-content-center align-items-end">
					<div class="col-sm col-12 mb-3 col-form-titles">
						<?php if ($title = opt('base_form_title')) : ?>
							<h2 class="form-title"><?= $title; ?></h2>
						<?php endif;
						if ($subtitle = opt('base_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $subtitle; ?></h3>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-9 col-12">
				<?php getForm('90'); ?>
			</div>
		</div>
	</div>
</div>
<?php
get_template_part('views/partials/repeat', 'form_base');
if ($fields['h_products']) {
	get_template_part('views/partials/content', 'posts',
			[
					'posts' => $fields['h_products'],
					'title' => $fields['h_products_title'] ? $fields['h_products_title'] : 'מוצרים מומלצים',
					'link' => $fields['h_products_link'],
			]);
}
if ($fields['h_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['h_slider_seo'],
			'img' => $fields['h_slider_img'],
	]);
}
if ($fields['h_gallery']) {
	get_template_part('views/partials/content', 'gallery',
			[
					'gallery' => $fields['h_gallery'],
					'title' => $fields['h_gallery_title'] ? $fields['h_gallery_title'] : 'הגלריה שלנו',
					'link' => $fields['h_gallery_link']
			]);
}
if ($fields['h_posts']) : ?>
	<div class="home-posts">
		<?php get_template_part('views/partials/content', 'posts',
				[
						'posts' => $fields['h_posts'],
						'title' => $fields['h_posts_title'] ? $fields['h_posts_title'] : 'קראו איתנו',
						'link' => $fields['h_posts_link']
				]); ?>
	</div>
<?php endif; ?>

<div class="container">
	<div class="row">
		<div class="col-12">
		</div>
	</div>
</div>


<?php if ($fields['single_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
