<?php
get_header();
$fields = get_fields();
get_template_part('views/partials/repeat', 'top_block', ['img' => opt('search_top')]);
?>
<div class="post-output-block article-page-body">
	<div class="container">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $s && $the_query_1->have_posts() ) { ?>
		<div class="row justify-content-center">
			<div class="col-auto">
				<h4 class="block-title">
					<?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?>
				</h4>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-md-6 col-12 col-post">
					<div class="post-card more-card" data-id="<?= get_the_ID(); ?>">
						<a class="post-item-image" href="<?= $link; ?>">
							<?php if (has_post_thumbnail()) : ?>
								<img src="<?= postThumb(); ?>" alt="post-image" class="post-image-card">
							<?php endif; ?>
						</a>
						<div class="post-card-content">
							<a class="post-card-title" href="<?= $link; ?>"><?php the_title(); ?></a>
							<p class="base-text">
								<?= text_preview(get_the_content(), 10); ?>
							</p>
							<a href="<?= $link; ?>" class="square-item post-card-link">
								<img src="<?= ICONS ?>arrow-black-left.png" alt="arrow-left">
							</a>
						</div>
					</div>
				</div>
			<?php }
			} if ($s && $the_query_2->have_posts()) { $the_query_2->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-md-6 col-12 col-post">
					<div class="post-card more-card" data-id="<?= get_the_ID(); ?>">
						<a class="post-item-image" href="<?= $link; ?>">
							<?php if (has_post_thumbnail()) : ?>
								<img src="<?= postThumb(); ?>" alt="post-image" class="post-image-card">
							<?php endif; ?>
						</a>
						<div class="post-card-content">
							<a class="post-card-title" href="<?= $link; ?>"><?php the_title(); ?></a>
							<p class="base-text">
								<?= text_preview(get_the_content(), 10); ?>
							</p>
							<a href="<?= $link; ?>" class="square-item post-card-link">
								<img src="<?= ICONS ?>arrow-black-left.png" alt="arrow-left">
							</a>
						</div>
					</div>
				</div>
			<?php } else { ?>
					<div class="row justify-content-center">
						<div class="col-auto pt-5">
							<h4 class="block-title">
								<?= esc_html__('שום דבר לא נמצא','leos'); ?>
							</h4>
						</div>
						<div class="col-12">
							<div class="alert alert-info text-center mb-5">
								<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
							</div>
						</div>
					</div>
			<?php }
			?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
