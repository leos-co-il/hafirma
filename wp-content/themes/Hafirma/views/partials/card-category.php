<?php if (isset($args['category']) && $args['category']) : $link = get_term_link($args['category']); ?>
	<div class="col-lg-4 col-md-6 col-12 category-col">
		<div class="card-category more-card" data-id="<?= $args['category']->term_id; ?>">
			<div class="category-img" <?php if ($img = get_field('cat_img', $args['category'])) : ?>
					style="background-image: url('<?= $img['url']; ?>')"
				<?php endif; ?>>
				<h3 class="category-card-title category-card-title-main"><?= $args['category']->name; ?></h3>
				<a class="category-card-content" href="<?= $link; ?>">
					<h3 class="category-card-title"><?= $args['category']->name; ?></h3>
					<p class="category-card-text">
						<?= text_preview(category_description($args['category']), 15); ?>
					</p>
					<span class="category-card-link" href="<?= $link; ?>">
						לקטגוריה
					</span>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
