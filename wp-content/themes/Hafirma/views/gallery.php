<?php
/*
Template Name: גלריה
*/

get_header();
$fields = get_fields();
$count = '';
get_template_part('views/partials/repeat', 'top_block', ['img' => $fields['top_img']]); ?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['single_gallery']) :  ?>
		<div class="gallery-body-output" id="gallery-block">
			<?php foreach ($fields['single_gallery'] as $x => $curr_image) : if (isset($curr_image['images']) && $curr_image['images']) : ?>
				<div class="page-home-gallery" dir="rtl">
					<?php foreach ($curr_image['images'] as $img_link) : ?>
						<div class="p-1 home-gallery-item">
							<a href="<?= $img_link['url']; ?>" class="gallery-main-item"
							   style="background-image: url('<?= $img_link['url']; ?>')" data-lightbox="gallery">
									<span class="square-item view-more">
										<img src="<?= ICONS ?>plus.png" alt="view-more">
									</span>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; endforeach; ?>
		</div>
	<?php endif; ?>
</article>
<div class="footer-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-center align-items-end">
					<div class="col-sm col-12 mb-3 col-form-titles">
						<?php if ($title = opt('base_form_title')) : ?>
							<h2 class="form-title"><?= $title; ?></h2>
						<?php endif;
						if ($subtitle = opt('base_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $subtitle; ?></h3>
						<?php endif; ?>
					</div>
					<div class="col-12">
						<?php getForm('89'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('views/partials/content', 'reviews',
		[
				'title' => $fields['reviews_title'],
				'subtitle' => $fields['reviews_subtitle'],
				'link' => $fields['reviews'],
				'top' => true
		]);
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
