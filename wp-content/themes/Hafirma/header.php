<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="drop-container">
	</div>
	<div class="float-search-form">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<?php if ($f_title = opt('search_form_title')) : ?>
						<h2 class="form-title"><?= $f_title; ?></h2>
					<?php endif;
					get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
    <div class="container-fluid">
        <div class="row justify-content-between align-items-center">
			<div class="col-auto d-flex justify-content-start align-items-center">
				<nav id="MainNav" class="h-100">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-menu', '3', '', 'main_menu h-100'); ?>
				</nav>
				<?php if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel">
						<img src="<?= ICONS ?>header-tel.png" alt="call-us">
						<span class="tel-num"><?= $tel; ?></span>
					</a>
				<?php endif; ?>
				<div class="search-trigger">
					<img src="<?= ICONS ?>search.png" alt="search">
				</div>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-auto">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="hafirma-logo">
					</a>
				</div>
			<?php endif; ?>
		</div>
    </div>
</header>

<div class="pop-great"></div>
<div class="triggers-col">
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href="<?= $whatsapp; ?>" class="square-item trigger-whatsapp-item">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
		</a>
	<?php endif; ?>
	<div class="square-item square-item-pop pop-trigger">
		<img src="<?= ICONS ?>mail.png" alt="contact-us">
	</div>
</div>
<div class="float-form">
	<div class="popup-form">
		<?php if ($popTitle = opt('pop_form_title')) : ?>
			<h2 class="form-title">
				<?= $popTitle; ?>
			</h2>
		<?php endif;
		if ($popText = opt('pop_form_subtitle')) : ?>
			<p class="form-subtitle">
				<?= $popText; ?>
			</p>
		<?php endif; ?>
		<?php getForm('86'); ?>
	</div>
</div>
