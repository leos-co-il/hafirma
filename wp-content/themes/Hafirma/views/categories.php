<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$cats = get_terms( [
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
	'number' => 6,
]);
$cats_all = get_terms( [
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
]);
get_template_part('views/partials/repeat', 'top_block', ['img' => $fields['top_img']]);
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="block-title">
					<?php the_title(); ?>
				</h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($cats) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($cats as $cat) {
					get_template_part('views/partials/card', 'category',
							[
									'category' => $cat,
							]);
				} ?>
			</div>
		<?php endif;
		if ($num = count($cats_all) > 6) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="product_cat" data-count="<?= $num; ?>">
						<span><?= esc_html__('טען עוד', 'leos'); ?></span>
						<img src="<?= ICONS ?>load-more.png" alt="load-more">
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
get_template_part('views/partials/content', 'reviews',
		[
				'title' => $fields['reviews_title'],
				'subtitle' => $fields['reviews_subtitle'],
				'link' => $fields['reviews'],
		]);
?>
<div class="form-home form-cats">
	<?php if ($img = opt('base_form_img')) : ?>
		<img src="<?= $img['url']; ?>" alt="image" class="base-form-img">
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-start">
			<div class="col-12">
				<div class="row justify-content-center align-items-end">
					<div class="col-sm col-12 mb-3 col-form-titles">
						<?php if ($title = opt('base_form_title')) : ?>
							<h2 class="form-title"><?= $title; ?></h2>
						<?php endif;
						if ($subtitle = opt('base_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $subtitle; ?></h3>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="col-xl-8 col-lg-9 col-12">
				<?php getForm('90'); ?>
			</div>
		</div>
	</div>
</div>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
