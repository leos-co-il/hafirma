<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$count = (isset($_REQUEST['countAll'])) ? $_REQUEST['countAll'] : '';
	$quantityNow = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : 'post';
	$termName = (isset($_REQUEST['termName'])) ? $_REQUEST['termName'] : 'category';
	$ids = explode(',', $ids_string);
	$query = '';
	if ($type !== 'product_cat') {
		$query = new WP_Query([
			'post_type' => $type,
			'posts_per_page' => 4,
			'post__not_in' => $ids,
			'suppress_filters' => false,
			'tax_query' => $id_term && $termName? [
				[
					'taxonomy' => $termName,
					'field' => 'term_id',
					'terms' => $id_term,
				]
			] : '',
		]);
	}
	$html = '';
	$result['html'] = '';
	if ($query && $query->have_posts() && $type !== 'product_cat') {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
			if ($count <= ($quantityNow + 4)) {
				$result['quantity'] = true;
			}
		}
	} elseif ($type === 'product_cat') {
		$cats_all = get_terms( [
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'exclude' => $ids
		]);
		if ($cats_all) {
			foreach ($cats_all as $item) {
				$html = load_template_part('views/partials/card', 'category', [
					'category' => $item,
				]);
				$result['html'] .= $html;
				if ($count <= ($quantityNow + 3)) {
					$result['quantity'] = true;
				}
			}
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}


add_action('wp_ajax_nopriv_order_function', 'order_function');
add_action('wp_ajax_order_function', 'order_function');

function order_function() {
	$order_type = (isset($_REQUEST['orderType'])) ? $_REQUEST['orderType'] : '';
	$termID = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$_price = '';
	$_price_desc = '';
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$ids = explode(',', $ids_string);
	$quantityNow = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	switch ($order_type) {
		case 'price' :
			$_price = true;
			$order_by = 'meta_value_num';
			$order = 'ASC';
			break;
		case 'price-desc' :
			$_price_desc = true;
			$order_by = 'meta_value_num';
			$order = 'DESC';
			break;
		case 'title' :
			$order_by = 'title';
			$order = 'ASC';
			break;
		case 'newest' :
			$order_by = 'date';
			$order = 'ASC';
			break;
		default :
			$order_by = 'title';
			$order = 'DESC';
	}
	$query_args = [
		'post_type' => 'product',
		'posts_per_page' => $quantityNow,
		'post__in' => $ids,
		'orderby' => $order_by ? $order_by : '',
		'order'   => $order ? $order : '',
		'tax_query' => $termID ? array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'term_id',
				'terms' => $termID,
			)
		) : '',
		'meta_query' => $_price || $_price_desc ? [
			[
				'key' => 'price',
			],
		] : '',
	];
	$query = new WP_Query($query_args);
	$html = '';
	$result['html'] = '';
	if ($query->have_posts()) {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
