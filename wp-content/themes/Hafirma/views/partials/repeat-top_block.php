<section>
	<div class="top-block" <?php if (isset($args['img']) && $args['img']) : ?>
		style="background-image: url('<?= $args['img']['url']; ?>')"
	<?php endif; ?>>
	</div>
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
