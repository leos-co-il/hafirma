<?php $reviews = (isset($args['reviews']) && $args['reviews']) ? $args['reviews'] : opt('reviews');
$title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('reviews_title');
$subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('reviews_subtitle');
$top = isset($args['top']) && $args['top'];
if ($reviews) : ?>
	<section class="reviews-block arrows-slider arrows-slider-base <?= $top ? 'top-revs' : ''; ?>" <?php if ($top) : ?>
	style="background-image: url('<?= IMG ?>reviews-top-back.png')"
	<?php endif; ?>>
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-4 col-sm-8 col-12 d-flex flex-column align-items-center justify-content-center mb-lg-0 mb-5">
					<?php if ($logo = opt('foo_logo')) : ?>
						<a class="logo reviews-logo"><img src="<?= $logo['url']; ?>" alt="logo-hafirma"></a>
					<?php endif; ?>
					<h2 class="category-card-title mb-2"><?= $title; ?></h2>
					<h3 class="review-title"><?= $subtitle; ?></h3>
				</div>
				<div class="col-lg-8 col-12">
					<div class="slider-content-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($reviews as $x => $review) : ?>
								<div class="review-slide">
									<div class="rev-content">
										<h3 class="review-title"><?= $review['name']; ?></h3>
										<h4 class="review-product-title"><?= $review['name_product']; ?></h4>
										<div class="base-output">
											<?= $review['text']; ?>
										</div>
										<?php if ($review['video']) : ?>
											<span class="square-item video-button play-button" data-video="<?= getYoutubeThumb($review['video'])?>">
												<img src="<?= ICONS ?>play.png" alt="play-video">
											</span>
											<div class="video-modal">
												<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
													 aria-hidden="true">
													<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
														<div class="modal-content">
															<div class="modal-body" id="iframe-wrapper"></div>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true" class="close-icon">×</span>
															</button>
														</div>
													</div>
												</div>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
