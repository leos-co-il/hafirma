<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$post_gallery = $fields['product_gallery'];
get_template_part('views/partials/repeat', 'top_block', ['img' => isset($fields['top_img']) ? $fields['top_img'] : '']);
?>
<article class="article-page-body page-body py-3">
	<div class="container">
		<div class="row col-title-mobile">
			<div class="col-12">
				<h1 class="main-product-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-xl-5 col-lg-6 col-12 d-flex flex-column align-items-start pt-lg-4 mt-0">
				<h1 class="main-product-title"><?php the_title(); ?></h1>
				<?php if ($fields['price'] || $fields['price_sale']) : ?>
					<div class="price-wrapper">
						<?php if ($fields['price']) : ?>
							<div class="old-price">
								<?= ' ₪'.$fields['price']; ?>
							</div>
						<?php endif;
						if ($fields['price_sale']) : ?>
							<div class="new-price">
								<?= ' ₪'.$fields['price_sale']; ?>
							</div>
						<?php endif; ?>
						<div class="product-call-link pop-trigger">מעוניינים? לחצו כאן!</div>
					</div>
				<?php endif; ?>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<div>
					<?php if ($fields['post_file']) : ?>
						<a class="share-items-line" download href="<?= $fields['post_file']['url']; ?>">
							<span class="square-item share-item">
								<img src="<?= ICONS ?>pdf.png" alt="download-file">
							</span>
							<span class="social-item-text">
								<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ? $fields['post_file']['title'] : 'הורידו את הקובץ'?>
							</span>
						</a>
					<?php endif; ?>
					<div class="share-items-line">
						<a class="square-item share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="square-item share-item">
							<img src="<?= ICONS ?>whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="social-item-text">
							שתף את המאמר
						</span>
					</div>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-lg-6 col-12 product-gallery-col arrows-slider">
					<div class="arrows-slider post-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							if ($post_gallery) : foreach ($post_gallery as $img): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; endif; ?>
						</div>
						<?php if ($post_gallery) : ?>
							<div class="thumbs-wrap">
								<div class="thumbs" dir="rtl">
									<?php if(has_post_thumbnail()): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
											   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
										</div>
									<?php endif; foreach ($post_gallery as $img): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
											   href="<?= $img['url']; ?>" data-lightbox="images-small">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="put-arrows-here"></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<div class="footer-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="row justify-content-center align-items-end">
					<div class="col-sm col-12 mb-3 col-form-titles">
						<?php if ($logo = opt('foo_logo')) : ?>
							<a class="logo foo-logo ml-3" href="/">
								<img src="<?= $logo['url']; ?>" alt="logo">
							</a>
						<?php endif;
						if ($title = opt('base_form_title')) : ?>
							<h2 class="form-title"><?= $title; ?></h2>
						<?php endif;
						if ($subtitle = opt('base_form_subtitle')) : ?>
							<h3 class="form-subtitle"><?= $subtitle; ?></h3>
						<?php endif; ?>
					</div>
					<div class="col-12">
						<?php getForm('89'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
if ($fields['post_gallery_slider']) {
	get_template_part('views/partials/content', 'gallery',
		[
			'gallery' => $fields['post_gallery_slider'],
			'title' => $fields['post_gallery_title'] ? $fields['post_gallery_title'] : 'הגלריה שלנו',
			'link' => $fields['post_gallery_link']
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
get_footer(); ?>
