<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$logo = opt('foo_logo');
$link_menu_title = opt('foo_menu_title');
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('foo_logo');
$id = get_the_ID();
$form_id = ($id === $contact_id) ? '91' : '88';
?>

<footer>
	<div>
		<div class="footer-form">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="row justify-content-center align-items-end">
							<div class="col-sm col-12 mb-3 col-form-titles">
								<?php if ($logo) : ?>
									<a class="logo foo-logo ml-3" href="/">
										<img src="<?= $logo['url']; ?>" alt="logo">
									</a>
								<?php endif;
								if ($title = opt('foo_form_title')) : ?>
									<h2 class="form-title"><?= $title; ?></h2>
								<?php endif;
								if ($subtitle = opt('foo_form_subtitle')) : ?>
									<h3 class="form-subtitle"><?= $subtitle; ?></h3>
								<?php endif; ?>
							</div>
							<div class="col-12">
								<?php getForm($form_id); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-main">
			<a id="go-top" class="square-item">
				<img src="<?= ICONS ?>to-top.png" alt="back-to-top">
			</a>
			<div class="container footer-container-menu">
				<div class="row justify-content-center">
					<div class="col-md-3 col-6 foo-menu">
						<h3 class="foo-title">תפריט ראשי</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '1'); ?>
						</div>
					</div>
					<div class="col-md col-12 foo-menu foo-menu-links">
						<h3 class="foo-title"><?= $link_menu_title ? $link_menu_title : 'אזור קידום'; ?></h3>
						<div class="menu-border-top">
							<?php getMenu('footer-links-menu', '1', 'two-columns'); ?>
						</div>
					</div>
					<div class="col-md-3 col-6 foo-contact-menu">
						<h3 class="foo-title">פרטי התקשרות </h3>
						<div class="menu-border-top">
							<ul class="contact-list">
								<?php  if ($address) : ?>
									<li>
										<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="foo-link">
											<?= $address; ?>
										</a>
									</li>
								<?php endif;
								if ($tel) : ?>
									<li>
										<a href="tel:<?= $tel; ?>" class="foo-link">
											<?= $tel; ?>
										</a>
									</li>
								<?php endif;
								if ($fax) : ?>
									<li>
										<div class="foo-link fax-item">
											<?= $fax; ?>
										</div>
									</li>
								<?php endif;
								if ($mail) : ?>
									<li>
										<a href="mailto:<?= $mail; ?>" class="foo-link">
											<?= $mail; ?>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
						<div class="foo-socials-line">
							<?php if ($instagram = opt('instagram')) : ?>
								<a href="<?= $instagram; ?>" class="square-item foo-social-link-item insta-link">
									<img src="<?= ICONS ?>instagram.png" alt="instagram">
								</a>
							<?php endif;
							if ($facebook = opt('facebook')) : ?>
								<a href="<?= $facebook; ?>" class="square-item foo-social-link-item">
									<img src="<?= ICONS ?>facebook.png" alt="facebook">
								</a>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
