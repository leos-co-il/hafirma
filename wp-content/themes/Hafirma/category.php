<?php

get_header();
$query = get_queried_object();
$fields = get_fields();
$posts = new WP_Query([
    'posts_per_page' => 8,
    'post_type' => 'post',
    'suppress_filters' => false,
	'tax_query' => [
			[
					'taxonomy' => 'category',
					'field' => 'term_id',
					'terms' => $query->term_id,
				]
		]
]);
$published_posts = new WP_Query([
    'posts_per_page' => -1,
    'post_type' => 'post',
    'suppress_filters' => false,
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				]
		]
]);
get_template_part('views/partials/repeat', 'top_block', ['img' => get_field('top_img', $query)]);
?>
<article class="page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="block-title">
                    <?= $query->name; ?>
				</h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center">
                    <?= category_description(); ?>
				</div>
			</div>
		</div>
        <?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
                <?php foreach ($posts->posts as $post) {
                    get_template_part('views/partials/card', 'post',
                        [
                            'post' => $post,
                        ]);
                } ?>
			</div>
        <?php endif;
        if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center mt-4">
				<div class="col-auto">
					<div class="more-link load-more-posts" data-type="post" data-count="<?= $num; ?>" data-term_name="category" data-term="<?= $query->term_id;?>">
						<span><?= esc_html__('טען עוד', 'leos'); ?></span>
						<img src="<?= ICONS ?>load-more.png" alt="load-more">
					</div>
				</div>
			</div>
        <?php endif; ?>
	</div>
</article>
<?php if ($gallery = get_field('post_gallery_slider', $query)) {
    get_template_part('views/partials/content', 'gallery',
        [
            'gallery' => $gallery,
            'title' => get_field('post_gallery_title', $query),
            'link' => get_field('post_gallery_link', $query)
        ]);
}
if ($seo = get_field('single_slider_seo', $query)) {
    get_template_part('views/partials/content', 'slider', [
        'content' => $seo,
        'img' => get_field('slider_img', $query),
    ]);
}
get_footer(); ?>

