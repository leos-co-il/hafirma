(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$('.pop-great').click(function() {
		$(this).removeClass('show-popup');
		$('.float-search-form').removeClass('show-search');
		$('.float-form').removeClass('show-float-form');
	});

	$('.pop-great').click(function(event){
		event.stopPropagation();
	});
	var linc2 = $('.drop-container'),
		timeoutId;
	$('.first-sub-menu').hover(function(){
		var content = $(this).children('.sub-menu').clone();
		if(content) {
			linc2.html(content);
			clearTimeout(timeoutId);
			linc2.show();
		}
	}, function(){
		timeoutId = setTimeout($.proxy(linc2,'hide'), 1000);
	});
	linc2.mouseenter(function(){
		clearTimeout(timeoutId);
	}).mouseleave(function(){
		linc2.hide();
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-great').toggleClass('show-popup');
			$('.float-search-form').toggleClass('show-search');
		});
		// $('.close-search').click(function () {
		// 	$('.pop-search').removeClass('show-search');
		// 	$('.float-search').removeClass('show-float-search');
		// });
		$('.pop-trigger').click(function () {
			$('.pop-great').toggleClass('show-popup');
			$('.float-form').toggleClass('show-float-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		var thumbsSlider = $('.thumbs');
		thumbsSlider.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		var prevAr = thumbsSlider.children('.slick-prev');
		var nextAr = thumbsSlider.children('.slick-next');
		$('.put-arrows-here').append(prevAr).append(nextAr);
		$('.base-gallery-slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			rtl: true,
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.page-home-gallery').each(function(i, obj) {
			$(obj).slick ({
				rtl:true,
				slidesToShow: 4,
				pauseOnHover:false,
				draggable:true,
				autoplay:true,
				dots:false,
				focusOnSelect:true,
				centerMode: true,
				centerPadding: '50px',
				fade:false,
				speed: 5000,
				autoplaySpeed: 3000,
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 3,
							centerPadding: '40px',
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							centerPadding: '30px',
						}
					},
					{
						breakpoint: 576,
						settings: {
							slidesToShow: 1,
						}
					},
				]
			});
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var termName = $(this).data('term_name');
		// var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
	var termID = $('#order').data('id');
	$('#order').on('change', function(e) {
		e.preventDefault();
		var idsOrder = '';
		$('.more-card').each(function(i, obj) {
			idsOrder += $(obj).data('id') + ',';
		});
		var quantity = $('.more-card').length;
		var orderType = $(this).find(':selected').val();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				ids: idsOrder,
				orderType: orderType,
				termID: termID,
				quantity: quantity,
				action: 'order_function',
			},
			success: function (data) {
				$('.put-here-products').html(data.html);
			}
		});
	});
	// $('#order-category').on('change', function(e) {
	// 	e.preventDefault();
	// 	var catId = $(this).find(':selected').val();
	// 	// var idsOrder = '';
	// 	// $('.more-card').each(function(i, obj) {
	// 	// 	idsOrder += $(obj).data('id') + ',';
	// 	// });
	// 	var quantity = $('.more-card').length;
	// 	jQuery.ajax({
	// 		url: '/wp-admin/admin-ajax.php',
	// 		dataType: 'json',
	// 		data: {
	// 			termID: catId,
	// 			quantity: quantity,
	// 			action: 'order_function',
	// 		},
	// 		success: function (data) {
	// 			$('.put-here-products').html(data.html);
	// 		}
	// 	});
	// });
})( jQuery );
