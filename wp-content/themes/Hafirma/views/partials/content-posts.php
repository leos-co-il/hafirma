<?php if (isset($args['posts']) && $args['posts']) :
$products = isset($args['products']) && $args['products'];
$title = $products ? 'מוצרים דומים' : 'מאמרים נוספים';
$link_title = $products ? 'מוצרים דומים' : 'מאמרים נוספים'; ?>
	<section class="posts-output">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= isset($args['title']) && $args['title'] ? $args['title'] : $title; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['posts'] as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
						]);
				} ?>
			</div>
			<?php if (isset($args['link']) && $args['link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $args['link']['url'];?>" class="base-link">
								<span class="link-text"><?= (isset($args['link']['title']) && $args['link']['title'])
											? $args['link']['title'] : $link_title;
									?></span>
							<img src="<?= ICONS ?>arrow-black-left.png" alt="arrow-left">
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
