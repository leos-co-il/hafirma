<?php
/*
Template Name: צור קשר
*/

get_header();
the_post();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$open_hours = opt('open_hours');
$address = opt('address');
$tel_2 = opt('tel_filial');
$mail_2 = opt('mail_filial');
$open_hours_2 = opt('address_filial');
$address_2 = opt('open_hours_filial');
?>
<article class="page-body page-body-contact">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1 class="block-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-10 col-12">
				<ul class="nav nav-tabs row justify-content-center" id="myTab" role="tablist">
					<li class="col-lg-auto col-sm-6 col-12 nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
							סניף חיפה, צ’ק פוסט
						</a>
					</li>
					<li class="col-lg-auto col-sm-6 col-12 nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
							סניף קריות, מיקום הסניף
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="row">
							<?php if ($tel) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png" alt="call-us">
									</div>
									<h4 class="contact-info">
										התקשרו אלינו
									</h4>
									<a href="tel:<?= $tel; ?>" class="contact-info-text">
										<?= $tel; ?>
									</a>
								</div>
							<?php endif;
							if ($address) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png" alt="visit-us">
									</div>
									<h4 class="contact-info">
										בקרו אותנו
									</h4>
									<a class="contact-info-text" href="https://www.waze.com/ul?q=<?= $address; ?>">
										<?= $address; ?>
									</a>
								</div>
							<?php endif;
							if ($mail) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png" alt="contact-mail">
									</div>
									<h4 class="contact-info">
										כיתבו לנו
									</h4>
									<a href="mailto:<?= $mail; ?>" class="contact-info-text">
										<?= $mail; ?>
									</a>
								</div>
							<?php endif;
							if ($open_hours) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.8s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-hours.png" alt="opening-hours">
									</div>
									<h4 class="contact-info">
										השעות שלנו
									</h4>
									<p class="contact-info-text">
										<?= $open_hours; ?>
									</p>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="row tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="row">
							<?php if ($tel_2) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png" alt="call-us">
									</div>
									<h4 class="contact-info">
										התקשרו אלינו
									</h4>
									<a href="tel:<?= $tel_2; ?>" class="contact-info-text">
										<?= $tel_2; ?>
									</a>
								</div>
							<?php endif;
							if ($address_2) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png" alt="visit-us">
									</div>
									<h4 class="contact-info">
										בקרו אותנו
									</h4>
									<a class="contact-info-text" href="https://www.waze.com/ul?q=<?= $address_2; ?>">
										<?= $address_2; ?>
									</a>
								</div>
							<?php endif;
							if ($mail_2) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png" alt="contact-mail">
									</div>
									<h4 class="contact-info">
										כיתבו לנו
									</h4>
									<a href="mailto:<?= $mail_2; ?>" class="contact-info-text">
										<?= $mail_2; ?>
									</a>
								</div>
							<?php endif;
							if ($open_hours_2) : ?>
								<div class="col-lg-3 col-sm-6 col-12 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.8s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-hours.png" alt="opening-hours">
									</div>
									<h4 class="contact-info">
										השעות שלנו
									</h4>
									<p class="contact-info-text">
										<?= $open_hours_2; ?>
									</p>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
