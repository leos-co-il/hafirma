<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$current_id = get_the_permalink();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$f_title = opt('post_form_title');
$f_subtitle = opt('post_form_subtitle');
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
get_template_part('views/partials/repeat', 'top_block', ['img' => $fields['top_img']]);
?>
<article class="article-page-body page-body py-3 mb-4">
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-xl-8 col-lg-7 col-12 d-flex flex-column align-items-start">
				<div class="base-output post-text-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<div>
					<?php if ($fields['post_file']) : ?>
						<a class="share-items-line" download href="<?= $fields['post_file']['url']; ?>">
							<span class="square-item share-item">
								<img src="<?= ICONS ?>pdf.png" alt="download-file">
							</span>
							<span class="social-item-text">
								<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ? $fields['post_file']['title'] : 'הורידו את הקובץ'?>
							</span>
						</a>
					<?php endif; ?>
					<div class="share-items-line">
						<a class="square-item share-item" href="mailto:?&subject=&body=<?= $current_id; ?>">
							<img src="<?= ICONS ?>mail.png" alt="share-by-email">
						</a>
						<a href="https://web.whatsapp.com/send?l=he&amp;phone=&amp;text=<?= $current_id; ?>" target="_blank"
						   class="square-item share-item">
							<img src="<?= ICONS ?>whatsapp.png" alt="share-by-whatsapp">
						</a>
						<span class="social-item-text">
							שתף את המאמר
						</span>
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12 position-relative">
				<div class="post-form-col">
					<?php if ($f_title && $f_subtitle) : ?>
						<div class="d-flex flex-column align-items-start">
							<h2 class="form-title"><?= $f_title; ?></h2>
							<h2 class="form-subtitle mb-4"><?= $f_subtitle; ?></h2>
						</div>
					<?php endif;
					getForm('87'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['post_gallery_slider']) {
	get_template_part('views/partials/content', 'gallery',
		[
			'gallery' => $fields['post_gallery_slider'],
			'title' => $fields['post_gallery_title'] ? $fields['post_gallery_title'] : 'הגלריה שלנו',
			'link' => $fields['post_gallery_link']
		]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'posts',
		[
			'posts' => $samePosts,
			'title' => $fields['same_title'] ? $fields['same_title'] : 'מאמרים דומים',
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $fields['single_slider_seo'],
		]);
}
get_footer(); ?>
